﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCards
{
	public partial class Fm : Form
	{
		private readonly Bitmap b;
		private readonly Graphics g;
		private readonly ImageBox imageBox;
		private readonly CardPack cardPack;
		private int CardCount = 5;

		public Fm()
		{
			InitializeComponent();

			b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
			g = Graphics.FromImage(b);

			imageBox = new ImageBox (Properties.Resources.cards, 4, 13);
			//imageBox = new ImageBox (Properties.Resources.cards2, 5, 13, 4*13 + 6);

			cardPack = new CardPack(imageBox.Count);   //массив индексов


			this.DoubleBuffered = true;
			this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

			this.Click += Fm_Click;

			


		}

		private void Fm_Click(object sender, EventArgs e)
		{
			//g.DrawImage(imageBox[3], 0, 0);

			Random rnd = new Random();
			//g.DrawImage(imageBox[rnd.Next(imageBox.Count)], 0, 0);
			g.Clear(SystemColors.Control);

			for (int i = 0; i < CardCount; i++)
			{
				g.DrawImage(imageBox[cardPack[rnd.Next(cardPack.Count)]], 
					rnd.Next(this.ClientSize.Width), rnd.Next(this.ClientSize.Height));
			}
			
			this.Invalidate();
		}
	}
}
//исправить размер картинки, если ширина и высота карт дробная (var w = image.Width / Cols;  var h = image.Height / Rows;)
//отдельный класс создать для хранения набора карт у игрока (там только индексы картинок)
//по нажатию F3 - веер (центр - нижний угол), F4 - случайный порядок (в случайном месте)
//лкм - перемещение веера (а сам веер не перерисовываем, только bitmap (можно создать новый bitmap))  нужно изменить метод click