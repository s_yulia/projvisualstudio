﻿using System;
using System.Linq;

namespace labLINQ
{
	class Program
	{
		static void Main(string[] args)
		{
			// пример 1

			//var arr = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			var arr = Enumerable.Range(0, 10).ToArray();

			var myQuery =
				from v in arr
				where v > 3 && v < 8
				orderby v descending     //сортировка в обр сторону
				select v*2;

			Console.WriteLine(string.Join(" ", arr));
			Console.WriteLine(string.Join(" ", myQuery));
			Console.WriteLine($"Count={myQuery.Count()}, Sum={myQuery.Sum()}");

			// пример 2

			var arr2 = new string[] { "Миша", "Юля", "Максим", "Сергей", "Олег", "Майкл" };

			var myQuery2 =
				from v in arr2
				where v.ToUpper().StartsWith("М")   //перевести в верх регистр и начин с буквы М
				orderby v     //сортировка
				select v;

			Console.WriteLine(string.Join(" ", myQuery2));

		}
	}
}
