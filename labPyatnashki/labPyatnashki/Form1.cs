﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPyatnashki
{
    public partial class FormMain : Form
    {
        //уровни сложности 3х3, 4х4, 5х5
        //механизм перемешивания с указанием кол-ва
        //управление мышкой и клавой + перемещение рядов
        //счетчик кол-ва ходов

       // private Button[,] but;
        private Game g;

        public FormMain()
        {
            InitializeComponent();

            g = new Game();
            g.Change += G_Change;
            g.DoReset(this);

            this.DoubleBuffered = true;
            buMix.Click += (s, e) => g.DoReset(this);
            buMix.Click += (s, e) => g.Mix_Buttons(this);
            buMix.Click += (s, e) => g.Start_Time();

        }

        private void G_Change(object sender, EventArgs e)
        {
            laStep.Text = $"Шаги: {g.Step}";
            laTime.Text = $"Время: {((g.Hours < 10) ? "0" : "")}{g.Hours}:{((g.Mins < 10) ? "0" : "")}{g.Mins}:{((g.Secs < 10) ? "0" : "")}{g.Secs}";
        }
    }
}

