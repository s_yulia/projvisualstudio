﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace labPyatnashki
{
    internal class Game 
    {
        public int Step { get; private set; }
        public int Rows { get; private set; } = 4;
        public int Cols { get; private set; } = 4;

        public int Hours { get; private set; } 
        public int Mins { get; private set; } 
        public int Secs { get; private set; }

        public Timer timer = new Timer();
        public Button[,] bu;
        public int s = 0;
        public int m = 0;
        public int h = 0;

        public event EventHandler Change;

        public Game()
        {
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
        }
        public void DoReset(Form fm)
        {
            Step = 0;
            Hours = 0;
            Mins = 0;
            Secs = 0;
            
            DeleteButtons(fm);
            CreateButtons(fm);
            ResizeButtons(fm);
            fm.Resize += (s, e) => ResizeButtons(fm);
     
        }

         void Timer_Tick(object sender, EventArgs e)
         {
            s++;
            if (s == 60)
            {
                m++;
                s = 0;
            }
            if (m == 60)
            {
                h++;
                m = 0;
            }
            Secs = s;
            Mins = m;
            Hours = h;

            Change?.Invoke(this, EventArgs.Empty);
        }

        public void Mix_Buttons(Form fm)
        {
            if (bu != null)
                for (int i = 0; i < Rows; i++)
                    for (int j = 0; j < Cols; j++)
                    {
                        bu[i, j].Text = null;
                    }
            Random rnd = new Random();
            bool flag = true;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[Rows - 1, Cols - 1].Text = (Rows * Cols).ToString();
                    do
                    {
                        flag = true;
                        int a = rnd.Next(1, 16);
                        if (bu[i,j].Text != (Cols * Rows).ToString())
                            foreach (var item in bu)
                            {
                                if (item != null)
                                    if (item.Text == a.ToString())
                                        flag = false;
                            }
                            if (flag)
                            {
                                if (bu[i,j].Text != (Cols * Rows).ToString())
                                    bu[i, j].Text = a.ToString();
                                bu[i, j].Enabled = true;
                                bu[i, j].Click += BuAll_Click;
                                bu[i, j].Click += (s, e) => Result_Check(fm);
                            }
                    } while (flag == false);
                }
            Change?.Invoke(this, EventArgs.Empty);
        }

        public void Start_Time()
        {
            timer.Stop();
            s = 0;
            m = 0;
            h = 0;
            timer.Start();
        }

        private void DeleteButtons(Form fm)
        {
            if (bu != null)
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Dispose();
                }
        }

        public void CreateButtons(Form fm)
        {
            int n = 1;
            bu = new Button[Rows, Cols];

            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    
                    bu[i, j] = new Button();
                    bu[i, j].ForeColor = Color.White;
                    bu[i, j].Text = n.ToString();
                    bu[i, j].Enabled = false;
                    bu[i, j].FlatStyle = FlatStyle.Flat;
                    bu[i, j].Font = new Font("Roboto", 23, FontStyle.Bold);
                    bu[i, j].BackColor = Color.FromArgb(153, 0, 0);
                    n++;
                    fm.Controls.Add(bu[i, j]);
                }

            bu[Rows - 1, Cols - 1].Visible = false;
            Change?.Invoke(this, EventArgs.Empty);
        }
          
        private void Result_Check(Form fm)
        {
            bool flag = true;
            int count = 1;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    if (bu[i, j].Text != count.ToString())
                    {
                        flag = false;
                    }
                    count++;
                }
            if (flag)
            {
                DialogResult choiceYesNo = MessageBox.Show("Ты выйграл. Начать заново?", "с:", MessageBoxButtons.YesNo);
                if (choiceYesNo == DialogResult.Yes)
                {
                    DoReset(fm);
                    Start_Time();
                }
                if (choiceYesNo == DialogResult.No)
                {
                    Application.Exit();
                }
            }
        }

        public void ResizeButtons(Form fm)
        {
            int xCellWidth = fm.ClientSize.Width / Cols - 1;
            int xCellHeight = (fm.ClientSize.Height - 100) / Rows - 1;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(j * xCellWidth, (i * xCellHeight) + 100); //X, Y
                }
        }

        private void BuAll_Click(object sender, EventArgs e)
        {
            if (sender is Button x)
            {
                int invisRow = 0;
                int invisCol = 0;
                Button tempX = null;

                for (int i = 0; i < Rows; i++)
                    for (int j = 0; j < Cols; j++)
                    {
                        if (bu[i, j].Visible == false)
                        {
                            invisRow = i;
                            invisCol = j;
                        }
                    }
                try 
                {
                    if ((x == bu[invisRow - 1, invisCol]) || (x == bu[invisRow + 1, invisCol]) || (x == bu[invisRow, invisCol + 1]) || (x == bu[invisRow, invisCol - 1]))
                        tempX = x;
                }
                catch (Exception)
                {
                    try 
                    { 
                        if ((x == bu[invisRow + 1, invisCol]) || (x == bu[invisRow - 1, invisCol]) ||  (x == bu[invisRow, invisCol + 1]) || (x == bu[invisRow, invisCol - 1]))
                            tempX = x;
                    }
                    catch (Exception)
                    {
                        try
                        {
                            if ((x == bu[invisRow, invisCol + 1]) || (x == bu[invisRow - 1, invisCol]) || (x == bu[invisRow + 1, invisCol]) ||  (x == bu[invisRow, invisCol - 1]))
                                tempX = x;
                        }
                        catch (Exception)
                        {

                            try
                            {
                                if ((x == bu[invisRow, invisCol - 1]) || (x == bu[invisRow, invisCol + 1]) || (x == bu[invisRow - 1, invisCol]) || (x == bu[invisRow + 1, invisCol]))
                                    tempX = x;
                            }
                            catch (Exception) { }
                        }
                    }
                }

                if (tempX != null)
                {
                    Step++;
                    string tempText = tempX.Text;
                    tempX.Visible = false;
                    tempX.Text = bu[invisRow, invisCol].Text;
                    bu[invisRow, invisCol].Visible = true;
                    bu[invisRow, invisCol].Text = tempText;
                    Change?.Invoke(this, EventArgs.Empty);
                }
                
            }
        }
    }
}
