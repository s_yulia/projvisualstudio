﻿namespace labPyatnashki
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.laStep = new System.Windows.Forms.Label();
			this.laTime = new System.Windows.Forms.Label();
			this.buMix = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// laStep
			// 
			this.laStep.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.laStep.AutoSize = true;
			this.laStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.laStep.Location = new System.Drawing.Point(30, 41);
			this.laStep.Name = "laStep";
			this.laStep.Size = new System.Drawing.Size(81, 31);
			this.laStep.TabIndex = 0;
			this.laStep.Text = "Шаги";
			// 
			// laTime
			// 
			this.laTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.laTime.AutoSize = true;
			this.laTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.laTime.Location = new System.Drawing.Point(247, 41);
			this.laTime.Name = "laTime";
			this.laTime.Size = new System.Drawing.Size(101, 31);
			this.laTime.TabIndex = 0;
			this.laTime.Text = "Время";
			// 
			// buMix
			// 
			this.buMix.BackColor = System.Drawing.Color.Crimson;
			this.buMix.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buMix.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.buMix.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.buMix.Location = new System.Drawing.Point(531, 21);
			this.buMix.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.buMix.Name = "buMix";
			this.buMix.Size = new System.Drawing.Size(192, 75);
			this.buMix.TabIndex = 1;
			this.buMix.Text = "Перемешать ";
			this.buMix.UseVisualStyleBackColor = false;
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(782, 1015);
			this.Controls.Add(this.buMix);
			this.Controls.Add(this.laTime);
			this.Controls.Add(this.laStep);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "FormMain";
			this.Text = "labPyatnashki";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laStep;
        private System.Windows.Forms.Label laTime;
        private System.Windows.Forms.Button buMix;
        private System.Windows.Forms.Timer timer1;
    }
}

