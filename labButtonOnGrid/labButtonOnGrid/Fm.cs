﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonOnGrid
{
	public partial class Fm : Form
	{
		private Button[,] bu;
		public int Rows { get; private set; } = 3;
		public int Cols { get; private set; } = 4;

		public Fm()
		{
			InitializeComponent();

			CreateButtons();

			ResizeButtons();

			this.Resize += (s, e) => ResizeButtons();    //когда меняется размер формы вызывается метод ресайз

		}

		private void ResizeButtons()
		{
			int xCellWidth = this.ClientSize.Width / Cols - 1;
			int xCellHeight = this.ClientSize.Height / Rows - 1;

			for (int i = 0; i < Rows; i++)
				for (int j = 0; j < Cols; j++)
				{
					bu[i, j].Width = xCellWidth;
					bu[i, j].Height = xCellHeight;
					bu[i, j].Location = new Point(j + (j * xCellWidth), i + (i * xCellHeight));  //X,Y
				}
		}

		private void CreateButtons()
		{
			bu = new Button[Rows, Cols]; //  (строка, столбец)
			for (int i = 0; i < Rows; i++)
			{
				for (int j = 0; j < Cols; j++)
				{
					bu[i, j] = new Button();
					bu[i, j].Text = $"{i * Cols + j}";
					bu[i, j].Font = new Font("Segoe UI", 20);
					bu[i, j].Click += BuAll_Click;	


					this.Controls.Add(bu[i, j]);
				}
			}
		}

		private void BuAll_Click(object sender, EventArgs e)
		{
			if (sender is Button x)
			{
				MessageBox.Show(x.Text);
			}
		}
	}
}


//При наведении изменение кнопки. Возможость редактирования количества кнопок.