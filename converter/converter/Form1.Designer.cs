﻿namespace converter
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbFrom = new System.Windows.Forms.ComboBox();
			this.cbTo = new System.Windows.Forms.ComboBox();
			this.btnConvert = new System.Windows.Forms.Button();
			this.tbFrom = new System.Windows.Forms.TextBox();
			this.tbTo = new System.Windows.Forms.TextBox();
			this.btnSwap = new System.Windows.Forms.Button();
			this.cbMetric = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cbFrom
			// 
			this.cbFrom.FormattingEnabled = true;
			this.cbFrom.Items.AddRange(new object[] {
            "mm",
            "cm",
            "dm",
            "m",
            "km"});
			this.cbFrom.Location = new System.Drawing.Point(81, 158);
			this.cbFrom.Name = "cbFrom";
			this.cbFrom.Size = new System.Drawing.Size(125, 28);
			this.cbFrom.TabIndex = 0;
			this.cbFrom.Text = "mm";
			// 
			// cbTo
			// 
			this.cbTo.FormattingEnabled = true;
			this.cbTo.Items.AddRange(new object[] {
            "mm",
            "cm",
            "dm",
            "m",
            "km"});
			this.cbTo.Location = new System.Drawing.Point(543, 158);
			this.cbTo.Name = "cbTo";
			this.cbTo.Size = new System.Drawing.Size(125, 28);
			this.cbTo.TabIndex = 1;
			this.cbTo.Text = "mm";
			// 
			// btnConvert
			// 
			this.btnConvert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			this.btnConvert.Font = new System.Drawing.Font("Showcard Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.btnConvert.Location = new System.Drawing.Point(262, 105);
			this.btnConvert.Name = "btnConvert";
			this.btnConvert.Size = new System.Drawing.Size(214, 44);
			this.btnConvert.TabIndex = 2;
			this.btnConvert.Text = "Конвертировать";
			this.btnConvert.UseVisualStyleBackColor = false;
			this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
			// 
			// tbFrom
			// 
			this.tbFrom.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.tbFrom.Location = new System.Drawing.Point(81, 115);
			this.tbFrom.Name = "tbFrom";
			this.tbFrom.Size = new System.Drawing.Size(125, 34);
			this.tbFrom.TabIndex = 3;
			this.tbFrom.Text = "1";
			// 
			// tbTo
			// 
			this.tbTo.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.tbTo.Location = new System.Drawing.Point(543, 115);
			this.tbTo.Name = "tbTo";
			this.tbTo.ReadOnly = true;
			this.tbTo.Size = new System.Drawing.Size(125, 34);
			this.tbTo.TabIndex = 4;
			// 
			// btnSwap
			// 
			this.btnSwap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			this.btnSwap.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.btnSwap.Location = new System.Drawing.Point(262, 158);
			this.btnSwap.Name = "btnSwap";
			this.btnSwap.Size = new System.Drawing.Size(214, 45);
			this.btnSwap.TabIndex = 5;
			this.btnSwap.Text = "<->";
			this.btnSwap.UseVisualStyleBackColor = false;
			this.btnSwap.Click += new System.EventHandler(this.btnSwap_Click);
			// 
			// cbMetric
			// 
			this.cbMetric.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.cbMetric.FormattingEnabled = true;
			this.cbMetric.Items.AddRange(new object[] {
            "длина",
            "вес",
            "время",
            "объем",
            "скорость"});
			this.cbMetric.Location = new System.Drawing.Point(302, 21);
			this.cbMetric.Name = "cbMetric";
			this.cbMetric.Size = new System.Drawing.Size(276, 39);
			this.cbMetric.TabIndex = 6;
			this.cbMetric.Text = "длина";
			this.cbMetric.SelectedIndexChanged += new System.EventHandler(this.cbMetric_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.White;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.label1.Location = new System.Drawing.Point(145, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(107, 38);
			this.label1.TabIndex = 7;
			this.label1.Text = "Меры:";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.ClientSize = new System.Drawing.Size(800, 261);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbMetric);
			this.Controls.Add(this.btnSwap);
			this.Controls.Add(this.tbTo);
			this.Controls.Add(this.tbFrom);
			this.Controls.Add(this.btnConvert);
			this.Controls.Add(this.cbTo);
			this.Controls.Add(this.cbFrom);
			this.Name = "Form1";
			this.Text = "Конвертер";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cbFrom;
		private System.Windows.Forms.ComboBox cbTo;
		private System.Windows.Forms.Button btnConvert;
		private System.Windows.Forms.TextBox tbFrom;
		private System.Windows.Forms.TextBox tbTo;
		private System.Windows.Forms.Button btnSwap;
		private System.Windows.Forms.ComboBox cbMetric;
		private System.Windows.Forms.Label label1;
	}
}

