﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace converter
{
	public partial class Form1 : Form
	{
		Dictionary<string, double> metrica; //ключ, значение
		public Form1()
		{
			InitializeComponent();
			metrica = new Dictionary<string, double>();

			//заполнение словаря
			metrica.Add("mm", 1);
			metrica.Add("cm", 10);
			metrica.Add("dm", 100);
			metrica.Add("m", 1000);
			metrica.Add("km", 1000000);
		}

		private void btnConvert_Click(object sender, EventArgs e)
		{
			if (tbFrom.Text != "")
			{
				double m1 = metrica[cbFrom.Text];
				double m2 = metrica[cbTo.Text];
				double n = Convert.ToDouble(tbFrom.Text);

				tbTo.Text = (n * m1 / m2).ToString();
			}
			else
				MessageBox.Show("Введите значение");
		}

		private void btnSwap_Click(object sender, EventArgs e)
		{
			string t = cbFrom.Text;
			cbFrom.Text = cbTo.Text;
			cbTo.Text = t;
		}

		private void cbMetric_SelectedIndexChanged(object sender, EventArgs e) //смена айтема
		{
			switch (cbMetric.Text)
			{
				case "длина":
					metrica.Clear();
					metrica.Add("mm", 1);
					metrica.Add("cm", 10);
					metrica.Add("dm", 100);
					metrica.Add("m", 1000);
					metrica.Add("km", 1000000);

					cbFrom.Items.Clear();
					cbFrom.Items.Add("mm");
					cbFrom.Items.Add("cm");
					cbFrom.Items.Add("dm");
					cbFrom.Items.Add("m");
					cbFrom.Items.Add("km");

					cbTo.Items.Clear();
					cbTo.Items.Add("mm");
					cbTo.Items.Add("cm");
					cbTo.Items.Add("dm");
					cbTo.Items.Add("m");
					cbTo.Items.Add("km");

					cbFrom.Text = "mm";
					cbTo.Text = "mm";
					break;

				case "вес":
					metrica.Clear();
					metrica.Add("g", 1);
					metrica.Add("kg", 1000);
					metrica.Add("t", 1000000);
					metrica.Add("lb", 454);


					cbFrom.Items.Clear();
					cbFrom.Items.Add("g");
					cbFrom.Items.Add("kg");
					cbFrom.Items.Add("t");
					cbFrom.Items.Add("lb");

					cbTo.Items.Clear();
					cbTo.Items.Add("g");
					cbTo.Items.Add("kg");
					cbTo.Items.Add("t");
					cbTo.Items.Add("lb");

					cbFrom.Text = "g";
					cbTo.Text = "g";
					break;
				case "время":
					metrica.Clear();
					metrica.Add("s", 1);
					metrica.Add("min", 60);
					metrica.Add("h", 3600);
					metrica.Add("day", 86400);
					metrica.Add("week", 604800);

					cbFrom.Items.Clear();
					cbFrom.Items.Add("s");
					cbFrom.Items.Add("min");
					cbFrom.Items.Add("h");
					cbFrom.Items.Add("day");
					cbFrom.Items.Add("week");

					cbTo.Items.Clear();
					cbTo.Items.Add("s");
					cbTo.Items.Add("min");
					cbTo.Items.Add("h");
					cbTo.Items.Add("day");
					cbTo.Items.Add("week");

					cbFrom.Text = "s";
					cbTo.Text = "s";
					break;
				case "объем":
					metrica.Clear();
					metrica.Add("l", 1);
					metrica.Add("ml", 0.001);
					metrica.Add("m^3", 1000);
					metrica.Add("gal", 3.7854117839999772);

					cbFrom.Items.Clear();
					cbFrom.Items.Add("l");
					cbFrom.Items.Add("ml");
					cbFrom.Items.Add("m^3");
					cbFrom.Items.Add("gal");

					cbTo.Items.Clear();
					cbTo.Items.Add("l");
					cbTo.Items.Add("ml");
					cbTo.Items.Add("m^3");
					cbTo.Items.Add("gal");

					cbFrom.Text = "l";
					cbTo.Text = "l";
					break;
				case "скорость":
					metrica.Clear();
					metrica.Add("m/s", 1);
					metrica.Add("km/h", 0.2778);
					metrica.Add("mile/h", 0.44704);

					cbFrom.Items.Clear();
					cbFrom.Items.Add("m/s");
					cbFrom.Items.Add("km/h");
					cbFrom.Items.Add("mile/h");

					cbTo.Items.Clear();
					cbTo.Items.Add("m/s");
					cbTo.Items.Add("km/h");
					cbTo.Items.Add("mile/h");

					cbFrom.Text = "m/s";
					cbTo.Text = "m/s";
					break;
				
				default:
					break;
			}
		}
	}
}
