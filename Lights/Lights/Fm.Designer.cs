﻿namespace Lights
{
	partial class Fm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buStart = new System.Windows.Forms.Button();
			this.laTime = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// buStart
			// 
			this.buStart.Location = new System.Drawing.Point(30, 12);
			this.buStart.Name = "buStart";
			this.buStart.Size = new System.Drawing.Size(94, 29);
			this.buStart.TabIndex = 0;
			this.buStart.Text = "Начать";
			this.buStart.UseVisualStyleBackColor = true;
			// 
			// laTime
			// 
			this.laTime.AutoSize = true;
			this.laTime.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.laTime.Location = new System.Drawing.Point(218, 9);
			this.laTime.Name = "laTime";
			this.laTime.Size = new System.Drawing.Size(69, 28);
			this.laTime.TabIndex = 1;
			this.laTime.Text = "Время";
			// 
			// Fm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.laTime);
			this.Controls.Add(this.buStart);
			this.Name = "Fm";
			this.Text = "Lights";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buStart;
		private System.Windows.Forms.Label laTime;
	}
}

