﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lights
{
	public partial class Fm : Form
	{


		private Game g;

		public Fm()
		{
			InitializeComponent();


			g = new Game();
			g.Change += G_Change;
			g.DoReset(this);

			this.DoubleBuffered = true;
			buStart.Click += (s, e) => g.DoReset(this);
			
			buStart.Click += (s, e) => g.Start_Time();
			buStart.Click += (s, e) => g.ColorLights();


		}

		private void G_Change ( object sender, EventArgs e ) 
		{

			laTime.Text = $"Время: {((g.Hours < 10) ? "0" : "")}{g.Hours}:{((g.Mins < 10) ? "0" : "")}{g.Mins}:{((g.Secs < 10) ? "0" : "")}{g.Secs}";

		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}
	}
}
