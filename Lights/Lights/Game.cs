﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Lights
{
	internal class Game
	
	{
		public int Hours { get; private set; }
		public int Mins { get; private set; }
		public int Secs { get; private set; }
		public Button[,] bu;
		public int Rows { get; private set; } = 4;
		public int Cols { get; private set; } = 4;

		public Timer timer = new Timer();
		public int Counter;

		public int repeatTimes = 1;


		public event EventHandler Change;

		public int s = 0;
		public int m = 0;

		
		

		public int h = 0;

		public Game()
		{
			timer.Interval = 1000;
			timer.Tick += Timer_Tick;
		}



		



		void Timer_Tick(object sender, EventArgs e)
		{
			s++;
			if (s == 60)
			{
				m++;
				s = 0;
			}
			if (m == 60)
			{
				h++;
				m = 0;
			}
			Secs = s;
			Mins = m;
			Hours = h;

			Change?.Invoke(this, EventArgs.Empty);
		}


		public void DoReset(Form fm)
		{
			
			Hours = 0;
			Mins = 0;
			Secs = 0;

			//DeleteButtons(fm);
			CreateButtons(fm);
			ResizeButtons(fm);
			fm.Resize += (s, e) => ResizeButtons(fm);

		}


		public void Start_Time()
		{
			timer.Stop();
			s = 0;
			m = 0;
			h = 0;
			timer.Start();
		}


		public void CreateButtons(Form fm)
		{
			int n = 0;
			bu = new Button[Rows, Cols];

			for (int i = 0; i < Rows; i++)
				for (int j = 0; j < Cols; j++)
				{

					bu[i, j] = new Button();
					bu[i, j].ForeColor = Color.White;
					bu[i, j].Text = n.ToString();
					bu[i, j].Enabled = false;
					bu[i, j].FlatStyle = FlatStyle.Flat;
					bu[i, j].Font = new Font("Roboto", 23, FontStyle.Bold);
					bu[i, j].BackColor = Color.White;
					
					fm.Controls.Add(bu[i, j]);
				}

			
			Change?.Invoke(this, EventArgs.Empty);
		}


		public void ResizeButtons(Form fm)
		{
			int xCellWidth = fm.ClientSize.Width / Cols - 20;
			int xCellHeight = (fm.ClientSize.Height - 100) / Rows - 20;
			for (int i = 0; i < Rows; i++)
				for (int j = 0; j < Cols; j++)
				{
					bu[i, j].Width = xCellWidth-50;
					bu[i, j].Height = xCellHeight-20;
					bu[i, j].Location = new Point(j * xCellWidth, (i * xCellHeight) + 100); //X, Y
				}
		}

		public  void ColorLights()
		{
			if (Counter != repeatTimes)
			{
					
						//bu[i, j].BackColor = SystemColors.GrayText;
						//bu[i, j].BackColor = Color.Green;
						//Counter++;
					
			}
			else
			{
				
						//bu[i, j].BackColor = SystemColors.GrayText;
						//timer.Stop();
			}
				

			Change?.Invoke(this, EventArgs.Empty);
		}



	}
}
