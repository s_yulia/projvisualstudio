﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace шашки
{
	public partial class Form1 : Form
	{

		const int mapSize = 8;  //кол-во клеток
		const int cellSize = 70; //размер клетки

		int[,] map = new int[mapSize, mapSize];

		Image whiteF;
		Image blackF;

		List<Button> simpleSteps = new List<Button>();

		int curPlayer; //тек игрок
		Button lastBtn; //последняя нажатая кнопка
		bool isMoving;

		int countEatSteps = 0; //кол-во возможных ходов
		Button presButton;
		bool isContinue = false; //можно ли сделать еще один ход

		Button[,] buttons = new Button[mapSize, mapSize];



		public Form1()
		{
			InitializeComponent();

			this.Text = "Шашки";

			whiteF = new Bitmap (new Bitmap(@"D:\переместить\разработка\шашки\шашки\шашки\pics\w.png"), new Size(cellSize-10, cellSize-10));
			blackF = new Bitmap (new Bitmap(@"D:\переместить\разработка\шашки\шашки\шашки\pics\b.png"), new Size(cellSize - 10, cellSize - 10));

			
			StartGame();
		}

		public void StartGame()
		{

			curPlayer = 1;
			isMoving = false;
			lastBtn = null;

			map = new int[mapSize, mapSize]
			{
				{0,1,0,1,0,1,0,1 },
				{1,0,1,0,1,0,1,0 },
				{0,1,0,1,0,1,0,1 },
				{0,0,0,0,0,0,0,0 },
				{0,0,0,0,0,0,0,0 },
				{2,0,2,0,2,0,2,0 },
				{0,2,0,2,0,2,0,2 },
				{2,0,2,0,2,0,2,0 }
			};

			CreateMap();

		}


		public void ResetGame()
		{
			bool player1 = false;
			bool player2 = false;

			for (int i = 0; i < mapSize; i++)
			{
				for (int j = 0; j < mapSize; j++)
				{
					if (map[i, j] == 1)
						player1 = true;
					if (map[i, j] == 2)
						player2 = true;
				}
			}
			if (!player1 || !player2) //закончились шашки и создаем новую игру
			{
				this.Controls.Clear();
				StartGame();
			}
		}

		public void CreateMap()
		{
			this.Width = (mapSize+1) * cellSize;
			this.Height = (mapSize+1) * cellSize;

			for (int i=0; i<mapSize; i++)
			{
				for (int j=0; j<mapSize; j++)
				{
					
					Button btn = new Button();
					btn.Location = new Point(j * cellSize, i * cellSize);
					btn.Size = new Size(cellSize, cellSize); //размер клетки
					btn.Click += new EventHandler(OnFigPress);
					if (map[i, j] == 1)
					{
						btn.Image = whiteF;
					}
					else if (map[i,j] == 2)
					{
						btn.Image = blackF;
					}
					btn.BackColor = GetPrevButtonColor(btn);
					btn.ForeColor = Color.Red;

					buttons[i, j] = btn;

					this.Controls.Add(btn); //добавление кнопки на форму
					
					

				}
			}

		}

		public void SwitchPlayer()
		{
			curPlayer = curPlayer == 1 ? 2 : 1;
			ResetGame();
		}

		public Color GetPrevButtonColor(Button lastBtn)  //меняет цвет пред кнопки
		{
			if ((lastBtn.Location.Y/cellSize % 2) != 0)
			{
				if ((lastBtn.Location.X / cellSize % 2) == 0)
				{
					return Color.DarkSlateGray;
				}
			}
			if ((lastBtn.Location.Y / cellSize % 2) == 0)
			{
				if ((lastBtn.Location.X / cellSize % 2) != 0)
				{
					return Color.DarkSlateGray;
				}
			}
			return Color.White;
		}
		public void OnFigPress(object sender, EventArgs e)
		{
			if(lastBtn != null)
			{
				lastBtn.BackColor = GetPrevButtonColor(lastBtn);
			}
			presButton = sender as Button; //получаем через сендер

			if(map[presButton.Location.Y/cellSize, presButton.Location.X/cellSize] != 0 && map[presButton.Location.Y / cellSize, presButton.Location.X / cellSize] == curPlayer)
			{
				CloseSteps();
				presButton.BackColor = Color.Red;
				DeactivAllButtons();
				presButton.Enabled = true;
				countEatSteps = 0;
				if (presButton.Text == "D")
					ShowSteps(presButton.Location.Y / cellSize, presButton.Location.X / cellSize, false);
				else ShowSteps(presButton.Location.Y / cellSize, presButton.Location.X / cellSize);

				if (isMoving)
				{
					CloseSteps();
					presButton.BackColor = GetPrevButtonColor(presButton);
					ShowPossibleSteps();
					isMoving = false;
				}
				else
					isMoving = true;
			}
			else
			{
				if (isMoving)
				{
					isContinue = false; //не продолжаем
					if (Math.Abs(presButton.Location.X / cellSize - lastBtn.Location.X / cellSize) > 1) //если разница в ходе больше 1 при съед ходе
					{
						isContinue = true; //мы продолжаем
						DeleteEaten(presButton, lastBtn);
					}
					//замена кнопок
					int temp = map[presButton.Location.Y / cellSize, presButton.Location.X / cellSize];
					map[presButton.Location.Y / cellSize, presButton.Location.X / cellSize] = map[lastBtn.Location.Y / cellSize, lastBtn.Location.X / cellSize];
					map[lastBtn.Location.Y / cellSize, lastBtn.Location.X / cellSize] = temp;
					presButton.Image = lastBtn.Image;
					lastBtn.Image = null;
					presButton.Text = lastBtn.Text;
					lastBtn.Text = "";

					SwitchButtonToCheat(presButton);//проверка на дамку
					countEatSteps = 0;
					isMoving = false;
					CloseSteps();
					DeactivAllButtons();
					if (presButton.Text == "D")
						ShowSteps(presButton.Location.Y / cellSize, presButton.Location.X / cellSize, false);
					else ShowSteps(presButton.Location.Y / cellSize, presButton.Location.X / cellSize);
					if (countEatSteps == 0 || !isContinue) //нет след съед ходов и	ход завершен
					{
						CloseSteps();
						SwitchPlayer();
						ShowPossibleSteps();
						isContinue = false;
					}
					else if (isContinue)
					{
						presButton.BackColor = Color.Red;
						presButton.Enabled = true;
						isMoving = true;
					}
				}
			}

			lastBtn = presButton;
		}



		public void ShowPossibleSteps()  //выделяет шашки у которых есть съедобный ход
		{
			bool isOneStep = true;
			bool isEatStep = false;
			DeactivAllButtons();
			for (int i = 0; i < mapSize; i++)
			{
				for (int j = 0; j < mapSize; j++)
				{
					if (map[i, j] == curPlayer)
					{
						if (buttons[i, j].Text == "D")
							isOneStep = false;
						else isOneStep = true;
						if (IsButtonsHasStep(i, j, isOneStep, new int[2] { 0, 0 }))
						{
							isEatStep = true;
							buttons[i, j].Enabled = true;
						}
					}
				}
			}
			if (!isEatStep)
				ActivAllButtons();
		}

		public void SwitchButtonToCheat(Button button)
		{
			if (map[button.Location.Y / cellSize, button.Location.X / cellSize] == 1 && button.Location.Y / cellSize == mapSize - 1) //1 игрок и позиция снизу
			{
				button.Text = "D";

			}
			if (map[button.Location.Y / cellSize, button.Location.X / cellSize] == 2 && button.Location.Y / cellSize == 0) //2 игрок и позиция сверху
			{
				button.Text = "D";
			}
		}
		public void DeleteEaten(Button endButton, Button startButton) //удаляем съеденные шашки
		{
			int count = Math.Abs(endButton.Location.Y / cellSize - startButton.Location.Y / cellSize); //расстояние между двумя кнопками
			int startIndexX = endButton.Location.Y / cellSize - startButton.Location.Y / cellSize;
			int startIndexY = endButton.Location.X / cellSize - startButton.Location.X / cellSize;
			startIndexX = startIndexX < 0 ? -1 : 1;
			startIndexY = startIndexY < 0 ? -1 : 1;
			int currCount = 0;
			int i = startButton.Location.Y / cellSize + startIndexX;
			int j = startButton.Location.X / cellSize + startIndexY;
			while (currCount < count - 1)
			{
				map[i, j] = 0;
				buttons[i, j].Image = null;
				buttons[i, j].Text = "";
				i += startIndexX;
				j += startIndexY;
				currCount++;
			}

		}

		public void ShowSteps(int iCurrFigure, int jCurrFigure, bool isOnestep = true)
		{
			simpleSteps.Clear();
			ShowDiagonal(iCurrFigure, jCurrFigure, isOnestep);
			if (countEatSteps > 0)
				closeSimpleSteps(simpleSteps);
		}
		public void ShowDiagonal(int IcurrFigure, int JcurrFigure, bool isOneStep = false)
		{
			int j = JcurrFigure + 1;
			for (int i = IcurrFigure - 1; i >= 0; i--)
			{
				if (curPlayer == 1 && isOneStep && !isContinue) break;
				if (InsideBorders(i, j))
				{
					if (!DeterminePath(i, j))
						break;
				}
				if (j < 7)
					j++;
				else break;

				if (isOneStep)
					break;
			}

			j = JcurrFigure - 1;
			for (int i = IcurrFigure - 1; i >= 0; i--)
			{
				if (curPlayer == 1 && isOneStep && !isContinue) break;
				if (InsideBorders(i, j))
				{
					if (!DeterminePath(i, j))
						break;
				}
				if (j > 0)
					j--;
				else break;

				if (isOneStep)
					break;
			}

			j = JcurrFigure - 1;
			for (int i = IcurrFigure + 1; i < 8; i++)
			{
				if (curPlayer == 2 && isOneStep && !isContinue) break;
				if (InsideBorders(i, j))
				{
					if (!DeterminePath(i, j))
						break;
				}
				if (j > 0)
					j--;
				else break;

				if (isOneStep)
					break;
			}

			j = JcurrFigure + 1;
			for (int i = IcurrFigure + 1; i < 8; i++)
			{
				if (curPlayer == 2 && isOneStep && !isContinue) break;
				if (InsideBorders(i, j))
				{
					if (!DeterminePath(i, j))
						break;
				}
				if (j < 7)
					j++;
				else break;

				if (isOneStep)
					break;
			}
		}


		public bool DeterminePath (int i1, int j1)
		{
			if (map[i1,j1] == 0 && !isContinue) //нулевые и первый съедобный ход
			{
				buttons[i1, j1].BackColor = Color.Yellow;
				buttons[i1, j1].Enabled = true;
				simpleSteps.Add(buttons[i1, j1]); //записываем в простые ходы
			}
			else
			{
				if (map[i1,j1] != curPlayer)
				{
					if (presButton.Text == "D")
						ShowProceduralEat(i1, j1, false);
					else ShowProceduralEat(i1, j1);
				}
				return false;
			}
			return true;
		}


		public void closeSimpleSteps(List<Button> simpleSteps)
		{
			if (simpleSteps.Count > 0)
			{
				for (int i=0; i < simpleSteps.Count; i++)
				{
					simpleSteps[i].BackColor = GetPrevButtonColor(simpleSteps[i]);
					simpleSteps[i].Enabled = false;
				}
			}
		}





		public void ShowProceduralEat(int i, int j, bool isOneStep = true)
		{
			int dirX = i - presButton.Location.Y / cellSize; //находим направление в котором сходили
			int dirY = j - presButton.Location.X / cellSize;

			dirX = dirX < 0 ? -1 : 1;
			dirY = dirY < 0 ? -1 : 1;

			int i1 = i;
			int j1 = j;
			bool isEmpty = true;
			while (InsideBorders(i1, j1))
			{
				if (map[i1,j1] != 0 && map[i1,j1] != curPlayer)
				{
					isEmpty = false;
					break;
				}
				i1 += dirX;
				j1 += dirY;

				if (isOneStep)
					break;
			}

			if (isEmpty)
				return;

			List<Button> toClose = new List<Button>();
			bool closeSimple = false; //нужно ли закрыть обычные ходы
			int ik = i1 + dirX;
			int jk = j1 + dirY;
			while (InsideBorders(ik,jk))
			{
				if (map[ik, jk] == 0) //нулевая ячейка
				{
					if (IsButtonsHasStep(ik, jk, isOneStep, new int[2] { dirX, dirY })) //если есть еще ходы
					{
						closeSimple = true; //закрываем простые ходы
					}
					else
					{
						toClose.Add(buttons[ik, jk]);
					}
					buttons[ik, jk].BackColor = Color.Yellow;
					buttons[ik, jk].Enabled = true;
					countEatSteps++;
				}
				else break;
				if (isOneStep) //дамка а мы смотрим на одну клетку
					break;
				jk += dirY;
				ik += dirX;
			}
			if (closeSimple && toClose.Count > 0) //если есть съедобные ходы, то закрываем простые
			{
				closeSimpleSteps(toClose);
			}

		}

		public bool IsButtonsHasStep(int IcurFig, int JcurFig, bool IsOneStep, int[] dir) //проверяем на шаги, 3парам - дамка, 4парам - направление
		{
			bool eatStep = false;
			int j = JcurFig + 1;
			for (int i = IcurFig - 1; i >= 0; i--)  //вверх вправо
			{
				if (curPlayer == 1 && IsOneStep && !isContinue) break; //1 игрок, дамка, и не может продолжать
				if (dir[0] == 1 && dir[1] == -1 && !IsOneStep) break; //если идет вниз влево 
				if (InsideBorders(i, j)) //внутри границ
				{
					if (map[i, j] != 0 && map[i, j] != curPlayer) //ненулев элемент и не равен тек игроку - есть ход
					{
						eatStep = true;
						if (!InsideBorders(i - 1, j + 1)) //если ход вправо вверх - вне границы
						{
							eatStep = false;
						}
						else if (map[i - 1, j + 1] != 0) //если за шашкой есть шашка
						{
							eatStep = false;
						}
						else return eatStep;
					}
				}
				if (j < 7)
				{
					j++;
				}
				else break;
				if (IsOneStep)
				{
					break;
				}

			}

			j = JcurFig - 1;
			for (int i = IcurFig - 1; i >= 0; i--) //вверх влево
			{
				if (curPlayer == 1 && IsOneStep && !isContinue) break; //1 игрок, дамка, и не может продолжать
				if (dir[0] == 1 && dir[1] == 1 && !IsOneStep) break; //если идет вниз вправо 
				if (InsideBorders(i, j)) //внутри границ
				{
					if (map[i, j] != 0 && map[i, j] != curPlayer) //ненулев элемент и не равен тек игроку - есть ход
					{
						eatStep = true;
						if (!InsideBorders(i - 1, j - 1)) //если ход влево вверх - вне границы
						{
							eatStep = false;
						}
						else if (map[i - 1, j - 1] != 0) //если за шашкой есть шашка
						{
							eatStep = false;
						}
						else return eatStep;
					}
				}
				if (j > 0)
					j--;
				else break;
				if (IsOneStep)
					break;
			}

			j = JcurFig - 1;
			for (int i = IcurFig + 1; i < 8; i++) //вниз 
			{
				if (curPlayer == 2 && IsOneStep && !isContinue) break; //1 игрок, дамка, и не может продолжать
				if (dir[0] == -1 && dir[1] == 1 && !IsOneStep) break; //если идет вверх вправо 
				if (InsideBorders(i, j)) //внутри границ
				{
					if (map[i, j] != 0 && map[i, j] != curPlayer) //ненулев элемент и не равен тек игроку - есть ход
					{
						eatStep = true;
						if (!InsideBorders(i + 1, j - 1)) //если ход влево вниз - вне границы
						{
							eatStep = false;
						}
						else if (map[i + 1, j - 1] != 0) //если за шашкой есть шашка
						{
							eatStep = false;
						}
						else return eatStep;
					}
				}
				if (j > 0)
					j--;
				else break;
				if (IsOneStep)
					break;
			}

			j = JcurFig + 1;
			for (int i = IcurFig + 1; i < 8; i++) //вниз 
			{
				if (curPlayer == 2 && IsOneStep && !isContinue) break; //1 игрок, дамка, и не может продолжать
				if (dir[0] == -1 && dir[1] == -1 && !IsOneStep) break; //если идет вверх влево 
				if (InsideBorders(i, j)) //внутри границ
				{
					if (map[i, j] != 0 && map[i, j] != curPlayer) //ненулев элемент и не равен тек игроку - есть ход
					{
						eatStep = true;
						if (!InsideBorders(i + 1, j + 1)) //если ход вправо вниз - вне границы
						{
							eatStep = false;
						}
						else if (map[i + 1, j + 1] != 0) //если за шашкой есть шашка
						{
							eatStep = false;
						}
						else return eatStep;
					}
				}
				if (j < 7)
					j++;
				else break;
				if (IsOneStep)
					break;
			}
			return eatStep;
		}

		public void CloseSteps() //закрываем шаги, которые были открыты для тек шашки
		{
			for (int i = 0; i < mapSize; i++)
			{
				for (int j = 0; j < mapSize; j++)
				{
					buttons[i, j].BackColor = GetPrevButtonColor(buttons[i,j]);  //присваиваем цвет, который был до этого
				}
			}
		}
		public bool InsideBorders(int _i,int _j) //находятся ли индексы в границах нашего массива
		{
			if(_i>=mapSize || _j>=mapSize || _i<0 || _j < 0)
			{
				return false;
			}
			return true;
		}
		public void ActivAllButtons()
		{
			for (int i = 0; i < mapSize; i++)
			{
				for (int j = 0; j < mapSize; j++)
				{
					buttons[i, j].Enabled = true;
				}
			}
		}

		public void DeactivAllButtons()
		{
			for(int i = 0; i < mapSize; i++)
			{
				for(int j = 0; j < mapSize; j++)
				{
					buttons[i, j].Enabled = false;
				}
			}
		}
	}

	
}
