﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageSelectInGrid
{
	public partial class Fm : Form
	{
		private const int rows_min = 2;
		private const int rows_max = 10;
		private const int cols_min = 2;
		private const int cols_max = 15;
		private readonly Bitmap b;
		private readonly Graphics g;
		private int cellWidth;
		private int cellHeight;
		private int curRow;
		private int curCol;
		private (int row, int col) selBegin;
		private (int row, int col) selEnd;

		public int Cols { get; private set; } = 5;
		public int Rows { get; private set; } = 4;

		public Fm()
		{
			InitializeComponent();

			b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
			g = Graphics.FromImage(b);

			this.DoubleBuffered = true;
			this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
			this.Resize += (s, e) => ResizeCells();

			this.MouseMove += Fm_MouseMove;
			this.MouseDown += Fm_MouseDown;
			this.KeyDown += Fm_KeyDown;

			this.Text = $"{Application.ProductName}: (F5/F6 - Rows, F7/F8 - Cols)";


			ResizeCells();


		}

		private void Fm_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				
				case Keys.F5:
					if (Rows > rows_min)
					{
						Rows--;
						ResizeCells();
					}
					break;
				case Keys.F6:
					if (Rows < rows_max)
					{
						Rows++;
						ResizeCells();
					}
					break;
				case Keys.F7:
					if (Cols > cols_min)
					{
						Cols--;
						ResizeCells();
					}
					break;
				case Keys.F8:
					if (Cols < cols_max)
					{
						Cols++;
						ResizeCells();
					}
					break;
				
			}
		}

		private void Fm_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{

				selBegin = selEnd =  (curRow, curCol);
				DrawCells();
			}
		}

		private void Fm_MouseMove(object sender, MouseEventArgs e)
		{
			for (int i = 0; i < Rows; curRow = i, i++)
				if (i * cellHeight > e.Y) break;

			for (int i = 0; i < Cols; curCol = i, i++)
				if (i * cellWidth > e.X) break;

			if (e.Button == MouseButtons.Left)
				selEnd = (curRow, curCol);

			DrawCells();
			this.Text = $"{selBegin.row}:{selBegin.col} - {selEnd.row}:{selEnd.col}";
		}

		private void ResizeCells()
		{
			cellWidth = this.ClientSize.Width / Cols;
			cellHeight = this.ClientSize.Height / Rows;
			DrawCells();

		}

		private void DrawCells()
		{
			//g.Clear(SystemColors.Control);
			g.Clear(DefaultBackColor);
			///////

			g.FillRectangle(new SolidBrush(Color.LightBlue),
				selBegin.col * cellWidth,
				selBegin.row * cellHeight,
				 (selEnd.col - selBegin.col + 1)*cellWidth,
				 (selEnd.row - selBegin.row + 1) * cellHeight    ///тут
				);


			for (int i = 0; i <= Rows; i++)
			{
				g.DrawLine(new Pen(Color.Green, 1), 0, i * cellHeight, Cols * cellWidth, i * cellHeight);
			}
			for (int i = 0; i <= Cols; i++)
			{
				g.DrawLine(new Pen(Color.Green, 1), i * cellWidth, 0, i * cellWidth, Rows * cellHeight);
			}

			g.DrawRectangle(new Pen(Color.Coral, 5),
				curCol * cellWidth, curRow * cellHeight, cellWidth, cellHeight);

			g.DrawString($"[{curRow}:{curCol}]", new Font("", 30), new SolidBrush(Color.Black), curCol * cellWidth, curRow * cellHeight);

			this.Invalidate();


		}
	}
}


//выделение снизу вверх 
//по правой кнопке мыши двигать все изображение
//изменение масштаба с помощью ролика мыши
