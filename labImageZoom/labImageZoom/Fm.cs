﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageZoom
{
	public partial class Fm : Form
	{
		private Point startPoint;

		Bitmap image;

		public int ZoomDelta { get; private set; } = 50;

		public Fm()
		{
			InitializeComponent();

			pxImage.MouseMove += PxImage_MouseMove;
			pxImage.MouseWheel += PxImage_MouseWheel;
			pxZoom.MouseDown += (s, e) => startPoint = e.Location;
			pxZoom.MouseMove += Fm_MouseMove;
			pxZoom.Paint += PxZoom_Paint;


			OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
			open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат загружаемого файла
			if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
			{
				try
				{
					image = new Bitmap(open_dialog.FileName);
					//вместо pictureBox1 укажите pictureBox, в который нужно загрузить изображение 
					this.pxImage.Size = image.Size;
					pxImage.Image = image;
					pxImage.Invalidate();
				}
				catch
				{
					DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
					"Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private void Fm_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{

				pxZoom.Left += e.X - startPoint.X;
				pxZoom.Top += e.Y - startPoint.Y;
				startPoint = e.Location;
				this.Invalidate();

			}
		}

		private void PxImage_MouseWheel(object sender, MouseEventArgs e)
		{
			ZoomDelta += e.Delta > 0 ? -2 : +2;
			pxZoom.Invalidate();
		}

		private void PxZoom_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.DrawImage(pxImage.Image,
				new Rectangle(0, 0, pxZoom.Width, pxZoom.Height),
				new Rectangle(startPoint.X - ZoomDelta, startPoint.Y - ZoomDelta, ZoomDelta *2, ZoomDelta *2),
				//new Rectangle(startPoint.X - 50, startPoint.Y - 50, 100, 100),
				GraphicsUnit.Pixel
				);
		}

		private void PxImage_MouseMove(object sender, MouseEventArgs e)
		{
			Text = $"{Application.ProductName} : ({e.X}, {e.Y})";
			startPoint = e.Location;
			if (pxImage.SizeMode == PictureBoxSizeMode.Zoom)
			{
				startPoint.X = e.X * pxImage.Image.Width / pxImage.Width;
				startPoint.Y = e.Y * pxImage.Image.Height / pxImage.Height;
			} else
			{
				startPoint = e.Location;
			}
			pxZoom.Invalidate();

		}
	}
}


//исправить ошибку с разными размерами картинки и формы
//потестировать работу с разными pxImage.SizeMode
//добавить ограничения на ZoomDelta
// добавить диалог выбор картинки - сделано
//добавить механизм перемещения компонента pxZoom по форме, мышкой (экранчик в другое место) - сделано
