﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
	public partial class Fm : Form
	{
		private Bitmap b;
		private Graphics g;
		private Point startPoint;
		private Pen myPen;

		public Fm()
		{
			InitializeComponent();

			b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
			g = Graphics.FromImage(b);
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			 

			myPen = new Pen(pxColor2.BackColor, 10);
			myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;



			pxImage.MouseDown += (s, e) => startPoint = e.Location;
			pxImage.MouseMove += PxImage_MouseMove;
			pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

			trPenWidth.Value = Convert.ToInt32(myPen.Width);
			trPenWidth.ValueChanged += (s, e) => myPen.Width = trPenWidth.Value;

			pxColor1.Click += (s, e) => myPen.Color = pxColor1.BackColor;
			pxColor2.Click += (s, e) => myPen.Color = pxColor2.BackColor;
			pxColor3.Click += (s, e) => myPen.Color = pxColor3.BackColor;
			pxColor4.Click += (s, e) => myPen.Color = pxColor4.BackColor;

			pxColorSelect.MouseClick += PxColorSelect_MouseClick;


			buImageClear.Click += delegate
			{
				g.Clear(SystemColors.Control);
				pxImage.Invalidate();
			};


			buImageSave.Click += BuImageSave_Click;
			buImageLoad.Click += BuImageLoad_Click;

			buAddRandomStars.Click += BuAddRandomStars_Click;

		}

		private void BuAddRandomStars_Click(object sender, EventArgs e)
		{
			var rnd = new Random();
			for (int i = 0; i < 100; i++)
			{
				g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))),
					rnd.Next(b.Width),
					rnd.Next(b.Height),
					rnd.Next(10),
					rnd.Next(10)
					);
				pxImage.Invalidate();
			}
		}

		private void BuImageLoad_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				b = (Bitmap) Bitmap.FromFile(dialog.FileName);
				g = Graphics.FromImage(b);
				pxImage.Invalidate();
			}
		}

		private void BuImageSave_Click(object sender, EventArgs e)
		{
			SaveFileDialog dialog = new SaveFileDialog();
			dialog.Filter = "JPG Files(*.JPG)|*.JPG";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				b.Save(dialog.FileName);
			}
		}

		private void PxColorSelect_MouseClick(object sender, MouseEventArgs e)
		{
			ColorDialog colorDialog = new ColorDialog();
			colorDialog.Color = pxColorSelect.BackColor;
			if(colorDialog.ShowDialog() == DialogResult.OK)
			{
				pxColorSelect.BackColor = colorDialog.Color;
				myPen.Color = pxColorSelect.BackColor;
			}
		}

		private void PxImage_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				g.DrawLine(myPen, startPoint, e.Location);
				startPoint = e.Location;
				pxImage.Invalidate();
			}
		}
	}
}


//делаем функционал похожий на обычный paint
//выбор цвета, изменение цвета, толщина, рисование по лкм и пкм
//нарисовать фигуры, заливка и тд

//рисовать и левой и правой км