﻿namespace labPaint
{
	partial class Fm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.buImageLoad = new System.Windows.Forms.Button();
			this.buImageSave = new System.Windows.Forms.Button();
			this.pxColorSelect = new System.Windows.Forms.PictureBox();
			this.buImageClear = new System.Windows.Forms.Button();
			this.trPenWidth = new System.Windows.Forms.TrackBar();
			this.pxColor4 = new System.Windows.Forms.PictureBox();
			this.pxColor3 = new System.Windows.Forms.PictureBox();
			this.pxColor2 = new System.Windows.Forms.PictureBox();
			this.pxColor1 = new System.Windows.Forms.PictureBox();
			this.pxImage = new System.Windows.Forms.PictureBox();
			this.buAddRandomStars = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pxColorSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.buAddRandomStars);
			this.panel1.Controls.Add(this.buImageLoad);
			this.panel1.Controls.Add(this.buImageSave);
			this.panel1.Controls.Add(this.pxColorSelect);
			this.panel1.Controls.Add(this.buImageClear);
			this.panel1.Controls.Add(this.trPenWidth);
			this.panel1.Controls.Add(this.pxColor4);
			this.panel1.Controls.Add(this.pxColor3);
			this.panel1.Controls.Add(this.pxColor2);
			this.panel1.Controls.Add(this.pxColor1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(340, 593);
			this.panel1.TabIndex = 0;
			// 
			// buImageLoad
			// 
			this.buImageLoad.Location = new System.Drawing.Point(13, 325);
			this.buImageLoad.Name = "buImageLoad";
			this.buImageLoad.Size = new System.Drawing.Size(317, 29);
			this.buImageLoad.TabIndex = 5;
			this.buImageLoad.Text = "Load from file";
			this.buImageLoad.UseVisualStyleBackColor = true;
			// 
			// buImageSave
			// 
			this.buImageSave.Location = new System.Drawing.Point(13, 275);
			this.buImageSave.Name = "buImageSave";
			this.buImageSave.Size = new System.Drawing.Size(317, 29);
			this.buImageSave.TabIndex = 4;
			this.buImageSave.Text = "Save to file";
			this.buImageSave.UseVisualStyleBackColor = true;
			// 
			// pxColorSelect
			// 
			this.pxColorSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
			this.pxColorSelect.Location = new System.Drawing.Point(11, 92);
			this.pxColorSelect.Name = "pxColorSelect";
			this.pxColorSelect.Size = new System.Drawing.Size(319, 46);
			this.pxColorSelect.TabIndex = 3;
			this.pxColorSelect.TabStop = false;
			// 
			// buImageClear
			// 
			this.buImageClear.Location = new System.Drawing.Point(11, 226);
			this.buImageClear.Name = "buImageClear";
			this.buImageClear.Size = new System.Drawing.Size(319, 29);
			this.buImageClear.TabIndex = 2;
			this.buImageClear.Text = "Clear";
			this.buImageClear.UseVisualStyleBackColor = true;
			// 
			// trPenWidth
			// 
			this.trPenWidth.Location = new System.Drawing.Point(13, 171);
			this.trPenWidth.Minimum = 1;
			this.trPenWidth.Name = "trPenWidth";
			this.trPenWidth.Size = new System.Drawing.Size(317, 56);
			this.trPenWidth.TabIndex = 1;
			this.trPenWidth.Value = 1;
			// 
			// pxColor4
			// 
			this.pxColor4.BackColor = System.Drawing.Color.Yellow;
			this.pxColor4.Location = new System.Drawing.Point(261, 12);
			this.pxColor4.Name = "pxColor4";
			this.pxColor4.Size = new System.Drawing.Size(69, 60);
			this.pxColor4.TabIndex = 0;
			this.pxColor4.TabStop = false;
			// 
			// pxColor3
			// 
			this.pxColor3.BackColor = System.Drawing.Color.Red;
			this.pxColor3.Location = new System.Drawing.Point(177, 13);
			this.pxColor3.Name = "pxColor3";
			this.pxColor3.Size = new System.Drawing.Size(69, 60);
			this.pxColor3.TabIndex = 0;
			this.pxColor3.TabStop = false;
			// 
			// pxColor2
			// 
			this.pxColor2.BackColor = System.Drawing.Color.Blue;
			this.pxColor2.Location = new System.Drawing.Point(93, 13);
			this.pxColor2.Name = "pxColor2";
			this.pxColor2.Size = new System.Drawing.Size(69, 60);
			this.pxColor2.TabIndex = 0;
			this.pxColor2.TabStop = false;
			// 
			// pxColor1
			// 
			this.pxColor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.pxColor1.Location = new System.Drawing.Point(11, 13);
			this.pxColor1.Name = "pxColor1";
			this.pxColor1.Size = new System.Drawing.Size(69, 60);
			this.pxColor1.TabIndex = 0;
			this.pxColor1.TabStop = false;
			// 
			// pxImage
			// 
			this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pxImage.Location = new System.Drawing.Point(340, 0);
			this.pxImage.Name = "pxImage";
			this.pxImage.Size = new System.Drawing.Size(660, 593);
			this.pxImage.TabIndex = 1;
			this.pxImage.TabStop = false;
			// 
			// button1
			// 
			this.buAddRandomStars.Location = new System.Drawing.Point(13, 377);
			this.buAddRandomStars.Name = "button1";
			this.buAddRandomStars.Size = new System.Drawing.Size(317, 29);
			this.buAddRandomStars.TabIndex = 6;
			this.buAddRandomStars.Text = "Add random stars";
			this.buAddRandomStars.UseVisualStyleBackColor = true;
			// 
			// Fm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1000, 593);
			this.Controls.Add(this.pxImage);
			this.Controls.Add(this.panel1);
			this.Name = "Fm";
			this.Text = "labPaint";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pxColorSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pxColor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TrackBar trPenWidth;
		private System.Windows.Forms.PictureBox pxColor4;
		private System.Windows.Forms.PictureBox pxColor3;
		private System.Windows.Forms.PictureBox pxColor2;
		private System.Windows.Forms.PictureBox pxColor1;
		private System.Windows.Forms.PictureBox pxImage;
		private System.Windows.Forms.Button buImageClear;
		private System.Windows.Forms.PictureBox pxColorSelect;
		private System.Windows.Forms.Button buImageLoad;
		private System.Windows.Forms.Button buImageSave;
		private System.Windows.Forms.Button buAddRandomStars;
	}
}

