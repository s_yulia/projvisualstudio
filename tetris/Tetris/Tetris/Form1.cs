﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{
	public partial class Form1 : Form
	{
		Shape currentShape;
		int size;
		int[,] map = new int[16, 8];
		public Form1()
		{
			InitializeComponent();
			StartInit();
			
		}
		
		public void StartInit()
		{
			size = 25;

			currentShape = new Shape(3, 0);

			this.KeyUp += new KeyEventHandler(keyFunc);

			TickTimer.Interval = 100;
			TickTimer.Tick += new EventHandler(update);

			Invalidate();
		}

		private void keyFunc(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Space:
					break;
				case Keys.Right:
					ResetArea();
					currentShape.MoveRight();
					Merge();
					Invalidate();
					break;
				case Keys.Left:
					ResetArea();
					currentShape.MoveLeft();
					Merge();
					Invalidate();
					break;

			}
		}

		private void update(object sender, EventArgs e)
		{
			ResetArea();
			currentShape.MoveDown();
			Merge();
			Invalidate();
		}

		public void Merge()  //синхронизация матрицы с картой
		{
			for (int i=currentShape.y; i< currentShape.y+3; i++)
			{
				for (int j=currentShape.x; j< currentShape.x+3; j++)
				{
					if (currentShape.matrix[i - currentShape.y, j - currentShape.x] != 0)
						map[i, j] = currentShape.matrix[i - currentShape.y, j - currentShape.x];
				}
			}
		}

		public void ResetArea()  //очищение
		{
			for (int i = currentShape.y; i < currentShape.y + 3; i++)
			{
				for (int j = currentShape.x; j < currentShape.x + 3; j++)
				{
					if (i>=0 && j>=0 && i<16 && j <8)
					map[i, j] = 0;
				}
			}
		}

		public void DrawMap(Graphics e)
		{
			for (int i = 0; i < 16; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					if (map[i, j] == 1)
					{
						e.FillRectangle(Brushes.Red, new Rectangle(50 + j * (size) + 1, 50 + i * (size) + 1, size - 1, size - 1));
					}
				}
			}

		}
		public void DrawGrid(Graphics g)
		{
			for (int i=0; i<=16; i++)
			{
				g.DrawLine(Pens.Black, new Point(50, 50 + i * size), new Point(50 + 8 * size, 50 + i * size));
			}
			for (int i = 0; i <= 8; i++)
			{
				g.DrawLine(Pens.Black, new Point(50 + i * size, 50), new Point(50 + i * size, 50 + 16 * size));
			}

		}

		private void OnPaint(object sender, PaintEventArgs e)
		{
			DrawGrid(e.Graphics);
		}
	}
}
