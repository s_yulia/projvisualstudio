﻿namespace labSoundPlayer
{
	partial class Fm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buPlay = new System.Windows.Forms.Button();
			this.buStop = new System.Windows.Forms.Button();
			this.buPlayLooping = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.buPlay.Location = new System.Drawing.Point(12, 12);
			this.buPlay.Name = "button1";
			this.buPlay.Size = new System.Drawing.Size(155, 36);
			this.buPlay.TabIndex = 0;
			this.buPlay.Text = "Play";
			this.buPlay.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.buStop.Location = new System.Drawing.Point(182, 12);
			this.buStop.Name = "button2";
			this.buStop.Size = new System.Drawing.Size(135, 36);
			this.buStop.TabIndex = 1;
			this.buStop.Text = "Stop";
			this.buStop.UseVisualStyleBackColor = true;
			// 
			// button3
			// 
			this.buPlayLooping.Location = new System.Drawing.Point(333, 12);
			this.buPlayLooping.Name = "button3";
			this.buPlayLooping.Size = new System.Drawing.Size(146, 36);
			this.buPlayLooping.TabIndex = 2;
			this.buPlayLooping.Text = "PlayLooping";
			this.buPlayLooping.UseVisualStyleBackColor = true;
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(12, 67);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(467, 51);
			this.button4.TabIndex = 3;
			this.button4.Text = "SystemSounds.Beep";
			this.button4.UseVisualStyleBackColor = true;
			// 
			// Fm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(826, 456);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.buPlayLooping);
			this.Controls.Add(this.buStop);
			this.Controls.Add(this.buPlay);
			this.Name = "Fm";
			this.Text = "labSoundPlayer";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button buPlay;
		private System.Windows.Forms.Button buStop;
		private System.Windows.Forms.Button buPlayLooping;
		private System.Windows.Forms.Button button4;
	}
}

