﻿namespace exam15
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.startGame = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.tbOk = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbFalse = new System.Windows.Forms.TextBox();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnFalse = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.MistyRose;
			this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
			this.label1.Location = new System.Drawing.Point(234, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(597, 54);
			this.label1.TabIndex = 0;
			this.label1.Text = "Игра: Совпадают ли фигуры?";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MistyRose;
			this.panel1.Controls.Add(this.startGame);
			this.panel1.Location = new System.Drawing.Point(12, 66);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1087, 538);
			this.panel1.TabIndex = 1;
			// 
			// startGame
			// 
			this.startGame.BackColor = System.Drawing.Color.Teal;
			this.startGame.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.startGame.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.startGame.Location = new System.Drawing.Point(870, 467);
			this.startGame.Name = "startGame";
			this.startGame.Size = new System.Drawing.Size(202, 53);
			this.startGame.TabIndex = 0;
			this.startGame.Text = "Начать игру!";
			this.startGame.UseVisualStyleBackColor = false;
			this.startGame.Click += new System.EventHandler(this.startGame_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.BackColor = System.Drawing.Color.White;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.label2.Location = new System.Drawing.Point(12, 628);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 25);
			this.label2.TabIndex = 2;
			this.label2.Text = "Очки: ";
			// 
			// tbOk
			// 
			this.tbOk.Location = new System.Drawing.Point(99, 627);
			this.tbOk.Name = "tbOk";
			this.tbOk.Size = new System.Drawing.Size(125, 27);
			this.tbOk.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.White;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.label3.Location = new System.Drawing.Point(835, 628);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(83, 25);
			this.label3.TabIndex = 4;
			this.label3.Text = "Ошибки:";
			// 
			// tbFalse
			// 
			this.tbFalse.Location = new System.Drawing.Point(936, 627);
			this.tbFalse.Name = "tbFalse";
			this.tbFalse.Size = new System.Drawing.Size(125, 27);
			this.tbFalse.TabIndex = 5;
			// 
			// btnOk
			// 
			this.btnOk.BackColor = System.Drawing.Color.PaleGreen;
			this.btnOk.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.btnOk.Location = new System.Drawing.Point(362, 617);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(170, 46);
			this.btnOk.TabIndex = 6;
			this.btnOk.Text = "Совпадают!";
			this.btnOk.UseVisualStyleBackColor = false;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnFalse
			// 
			this.btnFalse.BackColor = System.Drawing.Color.LightCoral;
			this.btnFalse.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.btnFalse.Location = new System.Drawing.Point(560, 617);
			this.btnFalse.Name = "btnFalse";
			this.btnFalse.Size = new System.Drawing.Size(170, 46);
			this.btnFalse.TabIndex = 7;
			this.btnFalse.Text = "Не совпадают!";
			this.btnFalse.UseVisualStyleBackColor = false;
			this.btnFalse.Click += new System.EventHandler(this.btnFalse_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Teal;
			this.ClientSize = new System.Drawing.Size(1111, 671);
			this.Controls.Add(this.btnFalse);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.tbFalse);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.tbOk);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button startGame;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbOk;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbFalse;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnFalse;
		public System.Windows.Forms.Panel panel1;
	}
}

