﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exam15
{
    public partial class Form1 : Form
    {
		//Game game = new Game();

		public Form1()
        {
            InitializeComponent();
			//panel1.BackgroundImage = new Bitmap(@"D:\переместить\разработка\exam15\exam15\fig.png");
			
			
			
            
        }

        int boxSize = 100;
        int mapSize = 3;
        int top_side = 50; //отступы
        int left_side = 150;
        int countOk = 1;
        int countFalse = 1;
        int[] array = new int[3]; //массив для определения кода квадрата
        int[] array1 = new int[3];

		public void generateBox()
		{

			int y;

			Random rnd = new Random();
			for (y = 0; y < mapSize; y++)
			{
				array[y] = rnd.Next(0, 2);
				array1[y] = array[y];
			}

			for (int x = 0; x < mapSize; x++)
				for (y = 0; y < mapSize; y++)
				{
					if (array[y] == 1)
					{
						PictureBox box = new PictureBox();
						box.BorderStyle = BorderStyle.FixedSingle;
						box.Location = new Point(x * boxSize + left_side, y * boxSize + top_side);
						box.BackColor = Color.Yellow;
						box.Size = new Size(boxSize, boxSize);
						panel1.Controls.Add(box);
						box.BringToFront();
					}

					else
					{
						PictureBox box = new PictureBox();
						box.BorderStyle = BorderStyle.FixedSingle;
						box.Location = new Point(x * boxSize + left_side, y * boxSize + top_side);
						box.BackColor = Color.DarkCyan;
						box.Size = new Size(boxSize, boxSize);
						panel1.Controls.Add(box);
						box.BringToFront();
					}

				}
			////////////////////////////
			for (y = 0; y < mapSize; y++)
			{
				array[y] = rnd.Next(0, 2);
			}

			for (int x = 0; x < mapSize; x++)
			{
				for (y = 0; y < mapSize; y++)
				{
					if (array[y] == 1)
					{
						PictureBox box = new PictureBox();
						box.BorderStyle = BorderStyle.FixedSingle;
						box.Location = new Point(x * boxSize + 2 * left_side + mapSize * boxSize, y * boxSize + top_side);
						box.BackColor = Color.Yellow;
						box.Size = new Size(boxSize, boxSize);
						panel1.Controls.Add(box);
						box.BringToFront();
					}

					else
					{
						PictureBox box = new PictureBox();
						box.BorderStyle = BorderStyle.FixedSingle;
						box.Location = new Point(x * boxSize + 2 * left_side + mapSize * boxSize, y * boxSize + top_side);
						box.BackColor = Color.DarkCyan;
						box.Size = new Size(boxSize, boxSize);
						panel1.Controls.Add(box);
						box.BringToFront();
					}

				}
			}


		}

		private void startGame_Click(object sender, EventArgs e)
		{
			tbOk.Text = null;
			tbFalse.Text = null;
			countOk = 1;
			countFalse = 1;
			generateBox();
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
            int counter = 0;
            for (int j = 0; j < mapSize; j++)
            {
                if (array[j] == array1[j])
                {
                    counter++;
                }
            }
            if (counter == 3)
            {
                tbOk.Text = $"{countOk}";
				countOk++;
				generateBox();
            }
            else
            {
                tbFalse.Text = $"{countFalse}";
				countFalse++;
				generateBox();
               
            }
        }

		private void btnFalse_Click(object sender, EventArgs e)
		{
            int counter = 0;
            for (int j = 0; j < mapSize; j++)
            {
                if (array[j] == array1[j])
                {
                    counter++;
                }
            }
            if (counter == 3)
            {
                tbFalse.Text = $"{countFalse}";
				countFalse++;
                generateBox();
            }
            else
            {
                tbOk.Text = $"{countOk}";
				countOk++;
				generateBox();
            }

        }
	}
}
