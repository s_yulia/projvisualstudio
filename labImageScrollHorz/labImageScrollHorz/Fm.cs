﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollHorz
{
	public partial class Fm : Form
	{
		private readonly Bitmap b;
		private readonly Graphics g;
		private Bitmap imBG;
		private Point startPoint;
		private int deltaX;

		public Fm()
		{
			InitializeComponent();

			imBG = Properties.Resources.background1;
			this.Height = imBG.Height;

			this.DoubleBuffered = true;

			b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
			g = Graphics.FromImage(b);

			this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };

			this.MouseDown += (s, e) => startPoint = e.Location;
			this.MouseMove += Fm_MouseMove;

			this.KeyDown += Fm_KeyDown;
		}

		private void Fm_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				
				case Keys.Left:
					UpdateDeltaX(2);
					break;
				case Keys.Right:
					UpdateDeltaX(-2);
					break;
			}
			this.Invalidate();
		}

		private void Fm_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				UpdateDeltaX(e.X - startPoint.X);
				startPoint = e.Location;
				this.Invalidate();

			}
		}

		private void UpdateDeltaX(int v)
		{
			this.Text = $"{Application.ProductName} : {deltaX}, {v}";
			deltaX += v;
			if (deltaX > 0)
			{
				deltaX -= imBG.Width;
			} else if (deltaX < -imBG.Width)
			{
				deltaX += imBG.Width;
			}
		}

		private void UpdateBG()
		{
			g.Clear(SystemColors.Control);
			for (int i = 0; i < 3; i++)
			{
				g.DrawImage(imBG, deltaX +  i * imBG.Width, 0);

			}
		}
	}
}


//добавить несколько фонов
//сейчас у нас 3 картинки, а нужно так чтобы эта 3 расчитывалась, "i<3", выбрать картинку фона с маленькой шириной
//добавить ускорение на перемещение фона по кнопкам, т.е. менять значение "-2" 
//добавить несколько фонов с разной скоростью (каждая часть кусты небо тучки движутся со своей скоростью)