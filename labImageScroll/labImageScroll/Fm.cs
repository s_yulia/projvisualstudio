﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScroll
{
	public partial class Fm : Form
	{
		private readonly Bitmap b;
		private Point curPoint;
		private Point startPoint;

		public int ZoomDelta { get; private set; } = 50;


		public Fm()
		{
			InitializeComponent();

			b = new Bitmap (Properties.Resources._2);

			this.DoubleBuffered = true;
			this.Paint += (s, e) => e.Graphics.DrawImage(b, curPoint);
			this.MouseDown += (s, e) => startPoint = e.Location;
			this.MouseMove += Fm_MouseMove;
		}

		private void Fm_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{

				curPoint.X += e.X - startPoint.X;
				curPoint.Y += e.Y - startPoint.Y;
				startPoint = e.Location;
				this.Invalidate();

			}
		}
	}
}


//изменять размер изображения по mouse scroll (+ ограничения)
//картинка увеличивается под курсором