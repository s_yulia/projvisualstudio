﻿using System;
using System.Collections.Generic;
using System.Text;

namespace labTrainerAccount1
{
    class Game
    {
        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public string CodeText { get; private set; }
		private bool AnswerCorrect;

		//   public bool AnswerCorrect { get; private set; }
		public event EventHandler Change;

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }

        private void DoContinue()
        {
            Random rnd = new Random();
            int xValue1 = rnd.Next(20);
            int xValue2 = rnd.Next(20);
            int xResult = xValue1 + xValue2;
            int xResultNew = xResult;
            if (rnd.Next(2) == 1)
                xResultNew += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
            AnswerCorrect = xResult == xResultNew;  //если они равны, то вернется тру
            CodeText = $"{xValue1} + {xValue2} = {xResultNew}";
            Change?.Invoke(this, EventArgs.Empty);

        }
        public void DoAnswer(bool v)
        {
            if (v == AnswerCorrect)
                CountCorrect++;
            else
                CountWrong++;
            DoContinue();
        }
    }
}
